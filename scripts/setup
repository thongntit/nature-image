#!/bin/sh

# This script and other scripts are sourced directly,
# therefore we need to avoid using conventional syntax.
#
# For example, we should use
#   BDIR="$SETUP_IMAGE_BUILDDIR" . "$SETUP_OECORE/openembedded-core/oe-init-build-env"
# instead of
#   . "$SETUP_OECORE/openembedded-core/oe-init-build-env" "$SETUP_IMAGE_BUILDDIR"

# NOTE:
#   This script can only be sourced from its directory.

SETUP_IMAGE="$IMAGE"
if [ -z "$SETUP_IMAGE" ]; then
    # Give it another try with $1
    SETUP_IMAGE="$1"
    if [ -z "$SETUP_IMAGE" ]; then
        echo "Usage:" >&2
        echo "    IMAGE=<image> . $0" >&2
        echo "    . $0 <image>" >&2
        exit 1
    fi
fi

NATURE_IMAGE_ROOT=$(readlink -f "$PWD/..")

SETUP_OECORE="$NATURE_IMAGE_ROOT/meta-oe-core"
SETUP_COMMON_CONF="$NATURE_IMAGE_ROOT/conf"
SETUP_IMAGE_CONF="$NATURE_IMAGE_ROOT/meta-image/meta-$SETUP_IMAGE/imgconf"
SETUP_IMAGE_BUILDDIR="$NATURE_IMAGE_ROOT/build/build-$SETUP_IMAGE"

for d in "$SETUP_OECORE" "$SETUP_COMMON_CONF" "$SETUP_IMAGE_CONF"; do
    if [ ! -e "$d" ]; then
        echo "$d not found" >&2
        return 1
    fi

    if [ ! -d "$d" ]; then
        echo "$d not a directory" >&2
        return 1
    fi
done


mkdir -p "$SETUP_IMAGE_BUILDDIR/conf"

# Create bblayers.conf
cat "$SETUP_COMMON_CONF/common.bblayers.conf" "$SETUP_IMAGE_CONF/bblayers.conf" > "$SETUP_IMAGE_BUILDDIR/conf/bblayers.conf"

# Create local.conf
cat "$SETUP_COMMON_CONF/common.local.conf" "$SETUP_IMAGE_CONF/local.conf" > "$SETUP_IMAGE_BUILDDIR/conf/local.conf"

# Create an empty templateconf.cfg
touch "$SETUP_IMAGE_BUILDDIR/conf/templateconf.cfg"

# Symlink bitbake
ln -sfr --no-dereference "$SETUP_OECORE/bitbake" "$SETUP_OECORE/openembedded-core/bitbake"

# Setting correct OEROOT here to prevent oe-init-build-env from guessing the wrong one.
OEROOT="$SETUP_OECORE/openembedded-core"
OEROOT=$(readlink -f "$OEROOT")

BDIR="$SETUP_IMAGE_BUILDDIR" . "$SETUP_OECORE/openembedded-core/oe-init-build-env"
